# health

<div dir="ltr">
  <br/>

  <p>
    This is healthcare android application.<br>
    In this application users can :<br>
    <ul>
      <li>Register daily <i>blood pressure</i> and <i>blood glucose</i></li>
      <li>Manage and calculate <i>calories in</i> foods and fruits</li>
      <li>Get report of <i>blood pressure</i> and <i>blood glucose</i> as list and chart</li>
      <li>Search about <i>medicines</i> and <i>illnesses</i> </li>
      <li>Manage daily medicine user must use and show them according to day</li>
    </ul>
  </p>
  
  <br><br>

<img src="/app_image/Screenshot_20200404-192437_%20.jpg" width="300" height="600" />
<img src="/app_image/Screenshot_20200404-192449_%20.jpg" width="300" height="600" />
<br/>
<img src="/app_image/Screenshot_20200404-192510_%20.jpg" width="300" height="600" />
<img src="app_image/Screenshot_20200404-192534_%20.jpg" width="300" height="600" />
<br/>
<img src="app_image/Screenshot_20200404-192555_%20.jpg" width="300" height="600" />
<img src="app_image/Screenshot_20200404-192519_%20.jpg" width="300" height="600" />


  <br/><br/><br/>
  
  <p> 
  I will be glad if you contribute 
    <ol>
      <li>Fork repository</li>
      <li>Clone repository</li>
      <li>Create a branch with suitable name and switch there</li>
      <li>Write your code and commit them</li>
      <li>Push your branch on server</li>
      <li>Send merge request to me</li>
    </ol>
  </p>
  
</div>  
